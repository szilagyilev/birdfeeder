//
//  Feeder+FoodType.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 19..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

enum FoodType: Int16, CaseIterable, CustomStringConvertible {
    case Other = 0
    case CrackedCorn = 1
    case Fruit = 2
    case Sunflower = 3
    case Worms = 4
    case Millet = 5
    case Milo = 6
    case Nyjer = 7
    case Oats = 8
    case Peanut = 9
    case Safflower = 10
    case Suet = 11
    case SugarWater = 12
    case FruitMix = 13
    case SeedMix = 14
    
    var description : String {
        switch self {
        case .Other:
            return "Other"
        case .CrackedCorn:
            return "Cracked Corn"
        case .Fruit:
            return "Fruit"
        case .Sunflower:
            return "Sunflower Seeds"
        case .Worms:
            return "Worms"
        case .Millet:
            return "Millet"
        case .Milo:
            return "Milo"
        case .Nyjer:
            return "Nyjer"
        case .Oats:
            return "Oats"
        case .Peanut:
            return "Peanut"
        case .Safflower:
            return "Safflower"
        case .Suet:
            return "Suet"
        case .SugarWater:
            return "Sugar Water"
        case .FruitMix:
            return "Mixed Fruits"
        case .SeedMix:
            return "Mixed Seeds"
        }
    }
}
