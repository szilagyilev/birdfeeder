//
//  UIImage+FeederType.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 17..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

enum FeederType: Int16, CaseIterable, CustomStringConvertible {
    case Ground = 0
    case SmallHopper = 1
    case LargeHopper = 2
    case SmallTube = 3
    case LargeTube = 4
    case Nectar = 5
    case Platform = 6
    case SuetCage = 7
    
    var description : String {
        switch self {
        case .Ground:
            return "Ground"
        case .SmallHopper:
            return "Small Hopper"
        case .LargeHopper:
            return "Large Hopper"
        case .SmallTube:
            return "Small Tube"
        case .LargeTube:
            return "Large Tube"
        case .Nectar:
            return "Nectar Feeder"
        case .Platform:
            return "Platform Feeder"
        case .SuetCage:
            return "Suet Cage"
        }
    }
}

extension UIImage{
    class func imageByFeederType(forType type: FeederType) -> UIImage {
        switch type {
        case .Ground:
            return UIImage(named: "Grass")!
        case .SmallHopper:
            return UIImage(named: "SmallHopper")!
        case .LargeHopper:
            return UIImage(named: "LargeHopper")!
        case .SmallTube:
            return UIImage(named: "SmallTube")!
        case .LargeTube:
            return UIImage(named: "LargeTube")!
        case .Nectar:
            return UIImage(named: "Nectar")!
        case .Platform:
            return UIImage(named: "Platform")!
        case .SuetCage:
            return UIImage(named: "SuetCage")!
        }
    }
}
