//
//  PersistencyError.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 16..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import Foundation

enum PersistencyError: Error {
    case managedContextNotFound
    case couldNotSaveObject
    case objectNotFound
}
