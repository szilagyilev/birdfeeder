//
//  ComponentBuilderProtocol.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 23..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

protocol ComponentBuilderProtocol {
    static func createComponent() -> UIViewController
}
