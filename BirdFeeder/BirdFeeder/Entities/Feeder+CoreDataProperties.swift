//
//  Feeder+CoreDataProperties.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 16..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import CoreData
import Foundation

extension Feeder {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Feeder> {
        return NSFetchRequest<Feeder>(entityName: "Feeder");
    }
    
    @NSManaged public var id: UUID
    @NSManaged public var name: String
    @NSManaged public var type: Int16
    @NSManaged public var foodType: Int16
    @NSManaged public var installDate: Date?
    @NSManaged public var lastFilledDate: Date?
    @NSManaged public var locationLatitude: Double
    @NSManaged public var locationLongitude: Double
    
}
