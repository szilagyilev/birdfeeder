//
//  FeederDetailsBuilder.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 23..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

class FeederDetailsBuilder {
    
}

extension FeederDetailsBuilder: ComponentBuilderSpecificProtocol {
    static func createComponent(forId iD: UUID?) -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "FeederDetailsController")
        if let view = viewController as? FeederDetailsViewController {
            let presenter: FeederDetailsPresenterProtocol = FeederDetailsPresenter()
            let wireFrame: FeederDetailsWireframeProtocol = FeederDetailsWireframe(viewController: view)
            let interactor: FeederDetailsInteractorProtocol = FeederDetailsInteractor()
            let localDataManager: FeederDetailsLocalDataManagerInputProtocol = FeederDetailsLocalDataManager()
            
            view.presenter = presenter
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            presenter.view = view
            presenter.feederId = iD
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            
            wireFrame.presenter = presenter
            
            return viewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
