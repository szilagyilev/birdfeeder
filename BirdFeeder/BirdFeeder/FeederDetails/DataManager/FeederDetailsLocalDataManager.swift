//
//  FeederDetailsDataManager.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 22..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import CoreData

class FeederDetailsLocalDataManager: FeederDetailsLocalDataManagerInputProtocol {
    func refillFeeder(withId iD: UUID) throws {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.managedContextNotFound
        }
        
        let request: NSFetchRequest<Feeder> = NSFetchRequest(entityName: String(describing: Feeder.self))
        
        let result = try managedOC.fetch(request)
        
        let feederWithId = result.first(where: { $0.id == iD })!
        
        feederWithId.lastFilledDate = Date()
        
        try managedOC.save()
    }
    
    func retrieveFeeder(withId iD: UUID) throws -> Feeder  {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.managedContextNotFound
        }
        
        let request: NSFetchRequest<Feeder> = NSFetchRequest(entityName: String(describing: Feeder.self))
        
        let result = try managedOC.fetch(request)
        
        return result.first(where: { $0.id == iD })!
    }
}
