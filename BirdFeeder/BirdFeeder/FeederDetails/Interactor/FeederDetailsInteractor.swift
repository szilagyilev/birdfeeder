//
//  FeederDetailsInteractor.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 22..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import Foundation

final class FeederDetailsInteractor {
    weak var presenter: FeederDetailsPresenterProtocol?
    var localDatamanager: FeederDetailsLocalDataManagerInputProtocol?
}

// MARK: - Extensions -

extension FeederDetailsInteractor: FeederDetailsInteractorProtocol {
    func refillFeeder(withId iD: UUID) throws {
        try localDatamanager?.refillFeeder(withId: iD)
    }
    
    func retrieveFeeder(withId iD: UUID) throws {
        presenter?.feeder =  try localDatamanager?.retrieveFeeder(withId: iD)
    }
}
