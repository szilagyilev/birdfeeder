//
//  FeederDetailsCell.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 19..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

protocol FeederDetailsCell {
    func set(forFeeder feeder: Feeder)
}
