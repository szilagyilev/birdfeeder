//
//  FeederDetailsTableViewFoodCell.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 19..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

class FeederDetailsTableViewFoodCell: UITableViewCell {
    
    @IBOutlet weak var foodTypeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var refillButton: UIButton!
    
}

extension FeederDetailsTableViewFoodCell: FeederDetailsCell {
    
    func set(forFeeder feeder: Feeder) {
        self.selectionStyle = .none
        titleLabel?.text = "Food"
        foodTypeLabel?.text = FoodType(rawValue: feeder.foodType)?.description
        refillButton.layer.cornerRadius = 10
    }
}
