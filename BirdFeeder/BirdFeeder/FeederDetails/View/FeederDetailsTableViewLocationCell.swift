//
//  FeederDetailsTableViewLocationCell.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 19..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import MapKit
import UIKit

class FeederDetailsTableViewLocationCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager: CLLocationManager!
}

extension FeederDetailsTableViewLocationCell: FeederDetailsCell {
    
    func set(forFeeder feeder: Feeder) {
        self.selectionStyle = .none
        titleLabel?.text = "Location"
        //let coordinates
        
        latitudeLabel?.text = "Latitude: \(feeder.locationLatitude)"
        longitudeLabel?.text = "Longitude: \(feeder.locationLongitude)"
        
        mapView.layer.cornerRadius = 10
        
        var mapRegion = MKCoordinateRegion()
        mapRegion.center = CLLocationCoordinate2DMake(feeder.locationLatitude, feeder.locationLongitude)
        mapRegion.span.latitudeDelta = 0.002
        mapRegion.span.longitudeDelta = 0.002
        
        mapView.setRegion(mapRegion, animated: true)
        
        let feederPin: MKPointAnnotation = MKPointAnnotation()
        feederPin.coordinate = mapRegion.center;
        
        mapView.addAnnotation(feederPin)
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: feeder.locationLatitude, longitude: feeder.locationLongitude), completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                return
            }
            
            if let places = placemarks {
                let pm = places[0] as CLPlacemark
                if (pm.thoroughfare != nil) {
                    self.titleLabel?.text = "Location near \(pm.thoroughfare!)"
                }
            }
        })
    }
}
