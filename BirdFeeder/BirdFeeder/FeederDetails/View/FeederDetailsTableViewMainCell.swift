//
//  FeederDetailsTableViewMainCell.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 19..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

class FeederDetailsTableViewMainCell: UITableViewCell {
    
    @IBOutlet weak var feederImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
}

extension FeederDetailsTableViewMainCell: FeederDetailsCell {
    
    func set(forFeeder feeder: Feeder) {
        self.selectionStyle = .none
        feederImageView?.image = UIImage.imageByFeederType(forType: FeederType(rawValue:feeder.type)!)
        titleLabel?.text = feeder.name
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy.MM.dd."
        let dateString = dateFormatter.string(from: feeder.lastFilledDate!)
        dateLabel?.text = "Last filled on \(dateString)"
    }
}
