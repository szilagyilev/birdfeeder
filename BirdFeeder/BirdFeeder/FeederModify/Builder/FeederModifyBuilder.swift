//
//  FeederModifyBuilder.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 23..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

class FeederModifyBuilder {
    
}

extension FeederModifyBuilder: ComponentBuilderSpecificProtocol {
    class func createComponent(forId iD: UUID?) -> UIViewController {
        let moduleViewController = mainStoryboard.instantiateViewController(withIdentifier:"FeederModifyController")
        
        if let view = moduleViewController as? FeederModifyViewController {
            let interactor: FeederModifyInteractorProtocol = FeederModifyInteractor()
            let presenter: FeederModifyPresenterProtocol = FeederModifyPresenter()
            let wireframe: FeederModifyWireframeProtocol = FeederModifyWireframe(viewController: view)
            let localDataManager: FeederModifyLocalDataManagerInputProtocol = FeederModifyLocalDataManager()
            
            view.presenter = presenter
            presenter.view = view
            presenter.feederId = iD
            presenter.wireframe = wireframe
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            
            wireframe.presenter = presenter
            
            return moduleViewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }

}
