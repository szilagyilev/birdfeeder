//
//  FeederModifyLocalDataManager.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 22..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import CoreData

class FeederModifyLocalDataManager:FeederModifyLocalDataManagerInputProtocol {
    
    func retrieveFeeder(withId iD: UUID) throws -> Feeder {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.managedContextNotFound
        }
        
        let request: NSFetchRequest<Feeder> = NSFetchRequest(entityName: String(describing: Feeder.self))
        
        let result = try managedOC.fetch(request)
        
        return result.first(where: { $0.id == iD })!
    }
    
    func saveFeeder(name: String, type: Int16, foodType: Int16, installDate: Date?, lastFilledDate: Date?, locationLatitude: Double, locationLongitude: Double) throws {
        
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.couldNotSaveObject
        }
        
        if let newFeeder = NSEntityDescription.entity(forEntityName: "Feeder",
                                                      in: managedOC) {
            let feeder = Feeder(entity: newFeeder, insertInto: managedOC)
            feeder.id = UUID()
            feeder.name = name
            feeder.type = type
            feeder.foodType = foodType
            feeder.installDate = installDate
            feeder.lastFilledDate = lastFilledDate
            feeder.locationLatitude = locationLatitude
            feeder.locationLongitude = locationLongitude
            try managedOC.save()
        }
    }
    
    func updateFeeder(id: UUID, name: String, type: Int16, foodType: Int16, installDate: Date?, lastFilledDate: Date?, locationLatitude: Double, locationLongitude: Double) throws {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.couldNotSaveObject
        }
        
        let request: NSFetchRequest<Feeder> = NSFetchRequest(entityName: String(describing: Feeder.self))
        
        let result = try managedOC.fetch(request)
        
        if let existingFeeder = result.first(where: { $0.id == id } ) {
            existingFeeder.name = name
            existingFeeder.type = type
            existingFeeder.foodType = foodType
            existingFeeder.locationLatitude = locationLatitude
            existingFeeder.locationLongitude = locationLongitude
            
            try managedOC.save()
        } else {
            throw PersistencyError.objectNotFound
        }
    }
}
