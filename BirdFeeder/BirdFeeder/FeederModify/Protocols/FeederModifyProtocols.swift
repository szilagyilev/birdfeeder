//
//  FeederModifyProtocols.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 17..
//  Copyright (c) 2018. szilagyilev. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import CoreLocation
import UIKit

protocol FeederModifyWireframeProtocol: WireframeProtocol {
    var presenter: FeederModifyPresenterProtocol? { get set }
    
    // PRESENTER -> WIREFRAME
    func closeFeederModifyScreen()
}

protocol FeederModifyViewProtocol: ViewProtocol {
    var presenter: FeederModifyPresenterProtocol? { get set }

    // PRESENTER -> VIEW
    func showFeederDetails(forFeeder feeder: Feeder?)
}

protocol FeederModifyPresenterProtocol: PresenterProtocol {
    var view: FeederModifyViewProtocol? { get set }
    var interactor: FeederModifyInteractorProtocol? { get set }
    var wireframe: FeederModifyWireframeProtocol? { get set }
    var feeder: Feeder? { get set }
    var feederId: UUID? { get set }
    var locationManager:CLLocationManager! { get set }

    // VIEW -> PRESENTER
    func viewDidLoad()
    func closeWithoutSaving()
    func saveNewFeeder(name: String, type: Int16, foodType: Int16, installDate: Date?, lastFilledDate: Date?, locationLatitude: Double, locationLongitude: Double) throws
    func saveAndCloseWithFeeder() throws

    // INTERACTOR -> PRESENTER
}

protocol FeederModifyInteractorProtocol: InteractorProtocol {
    var presenter: FeederModifyPresenterProtocol? { get set }
    var localDatamanager: FeederModifyLocalDataManagerInputProtocol? { get set }
    
    // PRESENTER -> INTERACTOR
    func retrieveFeeder(withId iD: UUID) throws
    func saveFeeder(name: String, type: Int16, foodType: Int16, installDate: Date?, lastFilledDate: Date?, locationLatitude: Double, locationLongitude: Double) throws
    func updateFeeder(feeder: Feeder?) throws
}

protocol FeederModifyLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
    func retrieveFeeder(withId iD: UUID) throws -> Feeder
    func saveFeeder(name: String, type: Int16, foodType: Int16, installDate: Date?, lastFilledDate: Date?, locationLatitude: Double, locationLongitude: Double) throws
    func updateFeeder(id: UUID, name: String, type: Int16, foodType: Int16, installDate: Date?, lastFilledDate: Date?, locationLatitude: Double, locationLongitude: Double) throws
}
