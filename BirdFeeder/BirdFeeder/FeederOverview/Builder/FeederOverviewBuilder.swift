//
//  FeederOverviewBuilder.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 23..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

class FeederOverviewBuilder {

}

extension FeederOverviewBuilder: ComponentBuilderProtocol {
    class func createComponent() -> UIViewController {
        let moduleViewController = mainStoryboard.instantiateViewController(withIdentifier:"FeederOverviewController")
        
        if let view = moduleViewController.children.first as? FeederOverviewViewController {
            let interactor: FeederOverviewInteractorProtocol = FeederOverviewInteractor()
            let presenter: FeederOverviewPresenterProtocol = FeederOverviewPresenter()
            let wireFrame: FeederOverviewWireframeProtocol = FeederOverviewWireframe(viewController: view)
            let localDataManager: FeederListLocalDataManagerInputProtocol = FeederListLocalDataManager()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            
            wireFrame.presenter = presenter
            
            return moduleViewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
