//
//  FeederListLocalDataManager.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 16..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import CoreData

class FeederListLocalDataManager:FeederListLocalDataManagerInputProtocol {
    
    func retrieveFeederList() throws -> [Feeder]  {
        
        //try createInitData()
        
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.managedContextNotFound
        }
        
        let request: NSFetchRequest<Feeder> = NSFetchRequest(entityName: String(describing: Feeder.self))
        
        return try managedOC.fetch(request)
    }
    
    func deleteFeeder(withId id:UUID) throws {
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.managedContextNotFound
        }
        
        let request: NSFetchRequest<Feeder> = NSFetchRequest(entityName: String(describing: Feeder.self))
        request.predicate = NSPredicate(format: "id = %@", id.uuidString)
        
        let feeders = try managedOC.fetch(request)
        let feederToDelete = feeders.first! as NSManagedObject
        
        managedOC.delete(feederToDelete)
        
        try managedOC.save()
        
    }
    
    func createInitData() throws{
        guard let managedOC = CoreDataStore.managedObjectContext else {
            return
        }
        
        if let newFeeder = NSEntityDescription.entity(forEntityName: "Feeder",
                                                      in: managedOC) {
            let feeder = Feeder(entity: newFeeder, insertInto: managedOC)
            feeder.id = UUID()
            feeder.name = "Riverside feeder"
            feeder.type = FeederType.LargeTube.rawValue
            feeder.foodType = FoodType.Sunflower.rawValue
            feeder.installDate = Date()
            feeder.lastFilledDate = Date()
            feeder.locationLatitude = 47.414843
            feeder.locationLongitude = 19.017324
            try managedOC.save()
        }
        
        if let newFeeder = NSEntityDescription.entity(forEntityName: "Feeder",
                                                      in: managedOC) {
            let feeder = Feeder(entity: newFeeder, insertInto: managedOC)
            feeder.id = UUID()
            feeder.name = "Garden feeder"
            feeder.type = FeederType.Platform.rawValue
            feeder.foodType = FoodType.Worms.rawValue
            feeder.installDate = Date()
            feeder.lastFilledDate = Date()
            feeder.locationLatitude = 47.414914
            feeder.locationLongitude = 19.018246
            try managedOC.save()
        }
    }
}
