//
//  FeederOverviewTableViewCell.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 16..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

class FeederOverviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var feederImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func set(forFeeder feeder: Feeder) {
        self.selectionStyle = .none
        feederImageView?.image = UIImage.imageByFeederType(forType: FeederType(rawValue:feeder.type)!)
        titleLabel?.text = feeder.name
        
    }
}
