//
//  FeederMapBuilder.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 23..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import UIKit

class FeederMapBuilder {
    
}

extension FeederMapBuilder: ComponentBuilderProtocol {
    static func createComponent() -> UIViewController {
        
        let moduleViewController = mainStoryboard.instantiateViewController(withIdentifier:"FeederMapController")
        
        if let view = moduleViewController.children.first as? FeederMapViewController {
            let interactor: FeederMapInteractorProtocol = FeederMapInteractor()
            let presenter: FeederMapPresenterProtocol = FeederMapPresenter()
            let wireFrame: FeederMapWireframeProtocol = FeederMapWireframe(viewController: view)
            let localDataManager: FeederMapLocalDataManagerInputProtocol = FeederMapLocalDataManager()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDataManager = localDataManager
            
            wireFrame.presenter = presenter
            
            return moduleViewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
