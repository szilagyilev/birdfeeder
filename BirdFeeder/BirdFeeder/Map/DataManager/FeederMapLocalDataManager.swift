//
//  FeederMapLocalDataManager.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 23..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import CoreData

class FeederMapLocalDataManager: FeederMapLocalDataManagerInputProtocol {
    
    func retrieveFeederList() throws -> [Feeder]  {
        
        guard let managedOC = CoreDataStore.managedObjectContext else {
            throw PersistencyError.managedContextNotFound
        }
        
        let request: NSFetchRequest<Feeder> = NSFetchRequest(entityName: String(describing: Feeder.self))
        
        return try managedOC.fetch(request)
    }
}
