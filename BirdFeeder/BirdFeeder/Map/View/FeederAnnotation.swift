//
//  FeederAnnotation.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 23..
//  Copyright © 2018. szilagyilev. All rights reserved.
//

import MapKit

class FeederAnnotation: NSObject, MKAnnotation {
    let title: String?
    let subTitle: String
    let coordinate: CLLocationCoordinate2D
    let feederId: UUID
    
    init(title: String, subTitle: String, coordinate: CLLocationCoordinate2D, feederId: UUID) {
        self.title = title
        self.subTitle = subTitle
        self.coordinate = coordinate
        self.feederId = feederId
        
        super.init()
    }
    
    var subtitle: String? {
        return subTitle
    }
}
