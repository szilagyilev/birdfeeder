//
//  TabBarWireFrameProtocol.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 16..
//  Copyright (c) 2018. szilagyilev. All rights reserved.
//

import UIKit

protocol TabBarWireFrameProtocol: class {
    static func createTabBarModule() -> UITabBarController
}
