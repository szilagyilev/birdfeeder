//
//  TabBarWireFrame.swift
//  BirdFeeder
//
//  Created by Szilagyi, Levente on 2018. 12. 16..
//  Copyright (c) 2018. szilagyilev. All rights reserved.
//

import UIKit

class TabBarWireFrame: TabBarWireFrameProtocol {
    
    class func createTabBarModule() -> UITabBarController {
        
        let tabbarController = mainStoryboard.instantiateViewController(withIdentifier:"FeederTabController") as! UITabBarController

        let viewControllers : [UIViewController] = [FeederOverviewBuilder.createComponent(), FeederMapBuilder.createComponent()]
        
        tabbarController.viewControllers = viewControllers
        
        if let feederList = tabbarController.tabBar.items?[0]{
            feederList.title = "Overview"
            feederList.image = UIImage(named: "TabBarList")
        }
        
        if let feederMap = tabbarController.tabBar.items?[1]{
            feederMap.title = "Map"
            feederMap.image = UIImage(named: "TabBarMap")
        }
//
//        if let postList = tabbarController.tabBar.items?[2]{
//            postList.title = "Settings"
//        }
        
        return tabbarController
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
